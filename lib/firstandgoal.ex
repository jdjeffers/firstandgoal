defmodule FirstAndGoal do
  @moduledoc """
  Documentation for FirstAndGoal.
  """

  @doc """
  Hello world.

  ## Examples

      iex> FirstAndGoal.hello()
      :world

  """
  def die(:black) do
    Enum.random([-5, -4, -3, -1, 0, 1])
  end

  def die(:red) do
    Enum.random([0, 1, 1, 2, 3, 5])
  end

  def die(:ivory) do
    Enum.random([1, 2, 3, 5, 6, 7])
  end

  def die(:brown) do
    Enum.random([3, 4, 5, 6, 7, 8])
  end

  def die(:yellow) do
    Enum.random([5, 6, 8, 9, 10, 10])
  end

  def die(:blue) do
    Enum.random([7, 8, 9, 10, 12, 14])
  end

  def die(:green) do
    Enum.random([10, 10, 10, 12, 14, 16])
  end

  def die(:play) do
    Enum.random([:X, :breakaway, :Hail, :Mary, :T, :penalty])
  end

  def die(:ref) do
    Enum.random([:X, :defense, :offense, :Hail, :Mary, :T])
  end

  def die(:penalty) do
    Enum.random([:five, :five, :fivea, :fivea, :ten, :fifteen])
  end

  def play(:sweep, :goalline) do
    [:black]
  end

  def play(_, _) do
    raise "Unknown play."
  end

  def results(dice) do
    results(dice, die(:play))
  end

  def results(dice, :breakaway) do
    result = results(dice, :breakaway)
    yards = yardage(dice)

    case die(:play) do
      :breakaway -> %{ result | yards: yards + result.yards }
      _ -> %{ yards: yards }
    end

  end

  def results(dice, :T) do
    yards = yardage(dice)

    case die(:ref) do
      :T -> %{ yards: yards, result: :turnover }
      _ -> %{ yards: yards }
    end
  end

  def results(dice, :penalty) do
    yards = yardage(dice)

    case die(:ref) do
      :offense -> %{ yards: yards, side: :offense, penalty: die(:penalty) }
      :defense -> %{ yards: yards, side: :defense, penalty: die(:penalty) }
      _ -> %{ yards: yards }
    end
  end

  def results(_, :X) do
    %{ yards: 0 }
  end

  def results(dice, _) do
    %{ yards: yardage(dice) }
  end

  def yardage(dice) do
    Enum.reduce dice, 0, fn(die_color, acc) ->
      acc + die(die_color)
    end
  end

  defmodule Position do
    defstruct side: nil, yardline: nil
  end

  defmodule State do
    defstruct team_a: "Team A", team_b: "Team B",
      ball_on: %Position{},
      team_a_score: 0, team_b_score: 0, possesion: nil, down: 0,
      first_down_at: %Position{},
      offensive_plays: [:sweep],
      defensive_plays: [:goalline],
      offense_deck: nil,
      offense_discard: [],
      defense_deck: nil,
      defense_discard: []

  end

  def kickoff() do
    results([:red, :ivory, :brown, :blue])
  end

  def driver do

    state = %State{}
    state = %{ state | offense_deck: Enum.shuffle(state.offensive_plays) }
    state = %{ state | defense_deck: Enum.shuffle(state.defensive_plays) }
    state = %{ state | possesion: Enum.random(["Team A", "Team B"]) }

    kickoff() |> apply(state)

  end

  def apply(yards, state) do

  end

end
